# -*- coding: utf-8 -*-
"""
Urls del proyecto
"""
from dajaxice.core import dajaxice_config, dajaxice_autodiscover
from django.conf.urls import patterns, include, url
# from django.contrib import admin
# admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

dajaxice_autodiscover()

urlpatterns = patterns(
    '',
    # url(r'^djangoadmin/', include(admin.site.urls)),
    # App urls
    url(r'^', include('apps.pipologin.urls')),
    url(r'^cuestionario/', include('apps.cuestionario.urls')),
    url(r'^alumno/', include('apps.alumno.urls')),
    url(r'^docente/', include('apps.docente.urls')),
    url(r'^admin/', include('apps.pipoadmin.urls')),
    url(r'^403/$', 'apps.pipoadmin.views.show_403'),
    url(r'^404/$', 'apps.pipoadmin.views.show_404'),
    # -- tutor
    url(r'^tutor/(?P<nombre_tutor>\w+)/$', 'apps.cuestionario.views.ver_tutor'),
    url(r'^tutordocente/(?P<nombre_tutor>\w+)/$',
        'apps.cuestionario.views.ver_tutor_docente'),
    #3rd party
    # (r'^tinymce/', include('tinymce.urls')),
    (dajaxice_config.dajaxice_url, include('dajaxice.urls')),
)
urlpatterns += staticfiles_urlpatterns()