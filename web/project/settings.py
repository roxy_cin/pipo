# -*- coding: utf-8 -*-
# Django settings for pipo.
import os

SELF_DIR = os.path.dirname(os.path.realpath(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG
ADMINS = (
    ('Cinthya Roxana Huacac Caballero', 'cinty@cinthyaroxana.com'),
)
MANAGERS = ADMINS
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'pipodb',
        'USER': 'root',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

TIME_ZONE = 'America/Lima'
LANGUAGE_CODE = 'es-PE'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'grappelli',
    # 'django.contrib.admin',
    # aplicaciones de 3ros
    'endless_pagination',
    'chartit',
    'dajaxice',
    'dajax',
    'south',
    'mail_templated',
    # aplicaciones locales
    'apps',
    'apps.common',
    'apps.alumno',
    'apps.docente',
    'apps.personas',
    'apps.cuestionario',
    'apps.pipoadmin',
    'apps.pipologin',
)
MEDIA_ROOT = ''
MEDIA_URL = ''
STATIC_ROOT = ''
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    '%s/../../static/' % SELF_DIR,
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    "dajaxice.finders.DajaxiceFinder"
)
SECRET_KEY = '_smc(^7spjr%=4@p6+3^cintytaw^@&amp;z394kkcqbay4qsglgt#'
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages'
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
ROOT_URLCONF = 'project.urls'
WSGI_APPLICATION = 'project.wsgi.application'
TEMPLATE_DIRS = ()
ENDLESS_PAGINATION_PER_PAGE = 15
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'pipo@cinthyaroxana.com'
EMAIL_HOST_PASSWORD = 'pipo123456'
EMAIL_PORT = 587