# -*- coding: UTF-8 -*-
"""
Urls de la aplicación cuestionario
"""

# 3rd party imports
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^$', 'apps.cuestionario.views.cuestionario_home'),
    url(r'^(?P<id_cuestionario>\d+)/$', 'apps.cuestionario.views.mostrar_cuestionario'),
    url(r'^guardar/$', 'apps.cuestionario.views.guardar_resultados'),
    url(r'^resultado/(?P<id_cuestionario>\d+)/$',
        'apps.cuestionario.views.ver_resultados'),
)