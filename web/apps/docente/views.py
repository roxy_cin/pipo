# -*- coding: utf-8 -*-
"""
Vistas de la aplicación docente
"""
import re
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Q, Avg
from django.http import Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
# locales
from apps.docente.forms import DocenteForm
from apps.cuestionario.forms import cuestionarioSeleccionarForm
from apps.cuestionario.models import cuestionarioAlumno, Resultado, Cuestionario
from apps.personas.models import Docente, Curso, Alumno, Matricula
from libs.utils import grafico_reporte


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='docente').count() == 1,
                  login_url='/')
def docente_home(request):
    """
    Pagina de inicio del módulo Docente
    """
    docente = Docente.objects.get(codigo=request.user)
    usuario_nombre = '%s, %s %s' % (docente.nombres,
                                    docente.apellido_paterno,
                                    docente.apellido_materno)
    data = {
        'usuario_nombre': usuario_nombre,
    }
    return render_to_response('docente_home.html', data)


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='docente').count() == 1,
                  login_url='/')
def docente_editar_perfil(request):
    """
    Editar datos del alumno
    """
    mensaje = ''
    # Recuperar el docente que inició sesión
    usuario = request.user
    docente = Docente.objects.get(codigo=usuario)
    # Mostrar formulario de edición de datos
    docente_form = DocenteForm(instance=docente)

    if request.method == 'POST':
        docente_form = DocenteForm(request.POST, instance=docente)
        if docente_form.is_valid():
            docente_form.save()
            mensaje = 'Datos actualizados correctamente'

    data = {
        'docente_form': docente_form,
        'mensaje': mensaje
    }
    return render_to_response('docente_editar_perfil.html', data,
                              context_instance=RequestContext(request))


@user_passes_test(lambda u: u.groups.filter(name='docente').count() == 1,
                  login_url='/')
def docente_editar_passwd(request):
    """
    Editar contraseña de docente
    """
    if request.method == 'POST':
        mensaje_passwd_er = mensaje_passwd_ok = None

        # Recuperar contraseña anterior
        password = request.POST.get('passwd_ant')
        # Verificar si es la contraseña correcta
        if authenticate(username=request.user.username, password=password):
            # recuperar la nueva contraseña ingresada
            nueva_passwd1 = request.POST.get('passwd_nueva1')
            nueva_passwd2 = request.POST.get('passwd_nueva2')
            if nueva_passwd1 == nueva_passwd2:
                user = request.user
                user.set_password(nueva_passwd1)
                user.save()
                mensaje_passwd_ok = u'Contraseña actualizada correctamente'
            else:
                mensaje_passwd_er = u'Las contraseñas ingresadas no coinciden'
        else:
            mensaje_passwd_er = u'La contraseña actual no es válida.'

        data = {
            'mensaje_passwd_er': mensaje_passwd_er,
            'mensaje_passwd_ok': mensaje_passwd_ok,
            'username': request.user.username,
            'plantilla': 'docente_base.html',
        }
        return render_to_response('cambia_password.html', data,
                                  context_instance=RequestContext(request))
    else:
        data = {
            'plantilla': 'docente_base.html'
        }
        return render_to_response('cambia_password.html', data,
                                  context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='docente').count() == 1,
                  login_url='/')
def lista_cursos(request):
    """
    Listar los cursos que dicta el docente
    """
    # Formulario de selección de cuestionario
    cuestionario_seleccionar_form = cuestionarioSeleccionarForm()
    # Ver el docente que esta usando el sistema
    docente = Docente.objects.get(codigo=request.user)
    # Recuperar lista de cursos que dicta
    # TODO: Listar cursos por matriculas, para obtener los semestres
    cursos = Curso.objects.filter(
        Q(codigo__in=[matricula.curso.codigo for matricula in
                      Matricula.objects.filter(docente=docente)]))

    data = {
        'cursos': cursos,
        'cuestionario_form': cuestionario_seleccionar_form,
    }

    return render_to_response('lista_cursos_docente.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='docente').count() == 1,
                  login_url='/')
def buscar_alumno(request):
    """
    Buscar alumno para ver el reporte de su cuestionario
    """
    alumnos = None
    mensaje_err = False
    cuestionario_seleccionar_form = cuestionarioSeleccionarForm()
    if request.method == 'POST':
        busqueda = request.POST.get('buscar_alumno')
        if re.match('\d+', busqueda):
            alumnos = Alumno.objects.filter(codigo__icontains=busqueda)
        elif re.match('\w+', busqueda):
            alumnos = Alumno.objects.filter(
                apellido_paterno__icontains=busqueda)

        if not alumnos:
            mensaje_err = True

    data = {
        'alumnos': alumnos,
        'cuestionario_form': cuestionario_seleccionar_form,
        'mensaje_err': mensaje_err
    }
    return render_to_response('buscar_alumno.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(name='docente').count() == 1,
                  login_url='/')
def reporte_alumno(request, codigo_alumno):
    """
    Ver reporte por alumno
    """
    try:
        alumno = Alumno.objects.get(codigo=codigo_alumno)
    except Alumno.DoesNotExist:
        alumno = None

    resultados = None
    mensaje = None
    cuestionario_lleno = None
    grafico_columna = None

    if request.method == 'POST':
        try:
            id_cuestionario = request.POST.get('cuestionario', '')
            # Recuperar cuestionario contestada
            cuestionario_lleno = cuestionarioAlumno.objects.get(
                cuestionario_id=id_cuestionario, alumno=alumno)
            # Mostrar los resultados
            resultados = Resultado.objects.filter(
                cuestionario_llena=cuestionario_lleno)
            grafico_columna = grafico_reporte(resultados, None, 'column')
        except cuestionarioAlumno.DoesNotExist:
            mensaje = 'No existen reportes para el alumno'

    print 'resul', resultados
    data = {
        'alumno': alumno,
        'resultados': resultados,
        'mensaje': mensaje,
        'cuestionario_lleno': cuestionario_lleno,
        'grafico_columna': grafico_columna
    }

    return render_to_response('reporte_alumno.html', data,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@user_passes_test(lambda u: u.groups.filter(
    Q(name='docente') | Q(name='administrador')).count() >= 1, login_url='/')
def reporte_curso_cuestionario(request, codigo_curso):
    """
    Ver reporte estadístico de curso
    """
    # Ver si es administrador o docente
    if request.user.groups.filter(name='administrador').count():
        plantilla = 'admin_base.html'
    elif request.user.groups.filter(name='docente').count():
        plantilla = 'docente_base.html'

    # Ver si se seleccionó una cuestionario para mostrar el reporte
    if request.method == 'POST':
        # Obtener el curso del cual mostrar reporte
        curso = Curso.objects.get(codigo=codigo_curso)

        # Recuperar la cuestionario para mostrar resultado
        cuestionario = Cuestionario.objects.get(
            id=request.POST.get('cuestionario'))
        # Obtener lista de alumnos que llevan el curso
        alumnos = Alumno.objects.filter(
            Q(codigo__in=[matricula.alumno.codigo for matricula in
                          Matricula.objects.filter(curso=curso)]),
            Q(codigo__in=[rep_cuestionario.alumno.codigo for rep_cuestionario in
                          cuestionarioAlumno.objects.filter(
                              cuestionario=cuestionario)]))

        if alumnos:
            # Obtener los resultados de los alumno seleccionadoos
            alumnos_seleccionados = request.POST.getlist('alumno_seleccionado')
            if alumnos_seleccionados:
                alumnos = Alumno.objects.filter(
                    Q(codigo__in=alumnos_seleccionados))

            id_cuestionario = request.POST.get('cuestionario', '')
            # Recuperar cuestionario contestada
            cuestionarios_llena = cuestionarioAlumno.objects.filter(
                Q(cuestionario_id=id_cuestionario), Q(alumno__in=alumnos))
            # Calcular los resultados para el gráfico
            resultados = Resultado.objects.filter(
                Q(cuestionario_llena__in=cuestionarios_llena))
            grafico = grafico_reporte(resultados, curso, 'column')

            resultados_tabla = resultados.values('categoria__nombre').annotate(
                Avg('cantidad'))
            print resultados_tabla

            data = {
                'curso': curso,
                'resultados_tabla': resultados_tabla,
                'cuestionario': cuestionario,
                'alumnos': alumnos,
                'id_cuestionario': id_cuestionario,
                'grafico': grafico,
                'plantilla': plantilla,
            }
        else:
            data = {
                'no_alumnos': True,
                'plantilla': plantilla,
            }
        return render_to_response('reporte_curso_cuestionario.html', data,
                                  context_instance=RequestContext(request))
    raise Http404
