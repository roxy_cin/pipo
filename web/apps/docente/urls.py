# -*- coding: UTF-8 -*-
"""
Urls de la aplicación cuestionario
"""

# 3rd party imports
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^$', 'apps.docente.views.docente_home'),
    url(r'^perfil/$', 'apps.docente.views.docente_editar_perfil'),
    url(r'^perfil/passwd/$', 'apps.docente.views.docente_editar_passwd'),
    # -- reportes
    url(r'^lista/cursos/$', 'apps.docente.views.lista_cursos'),
    url(r'^buscar/alumno/$', 'apps.docente.views.buscar_alumno'),
    url(r'^reporte/curso/(?P<codigo_curso>\w+)/*$',
        'apps.docente.views.reporte_curso_cuestionario'),
    url(r'^reporte/alumno/(?P<codigo_alumno>\d+)/$',
        'apps.docente.views.reporte_alumno'),
)