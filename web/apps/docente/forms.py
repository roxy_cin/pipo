# -*-  coding: utf-8 -*-
"""
Formulario para docentes
"""

from django.forms.models import ModelForm
from django.forms.widgets import TextInput, HiddenInput
from apps.personas.models import Docente


class DocenteFormAdmin(ModelForm):
    """
    Formulario del administrador para editar datos de docente
    """

    class Meta:
        model = Docente
        widgets = {
            'codigo': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'nombres': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;'
                }
            ),
            'apellido_paterno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;'
                }
            ),
            'apellido_materno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;'
                }
            ),
            'usuario': HiddenInput()
        }


class DocenteFormAgregar(ModelForm):
    """
    Formulario para agregar docente al sistema
    """

    class Meta:
        model = Docente
        widgets = {
            'codigo': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True,
                    'onKeyUp': "buscarDocente();"
                }
            ),
            'nombres': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            ),
            'apellido_paterno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            ),
            'apellido_materno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True,
                }
            ),
        }
        exclude = 'usuario'


class DocenteForm(ModelForm):
    """
    Formulario para que el docente edite sus datos
    """

    class Meta:
        model = Docente
        widgets = {
            'codigo': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'apellido_paterno': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'apellido_materno': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'nombres': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'usuario': HiddenInput()
        }
