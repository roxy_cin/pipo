# -*- coding: utf-8 -*-
from django import forms
from django.forms.widgets import TextInput, PasswordInput


class LoginForm(forms.Form):
    user = forms.CharField(
        max_length=100,
        widget=TextInput(
            attrs={
                'placeholder': 'Usuario',
                'autofocus': 'autofocus'
            }
        )
    )
    password = forms.CharField(
        max_length=100, widget=PasswordInput(
            attrs={'placeholder': u'Contraseña'}))