# -*-  coding: utf-8 -*-
"""
Ajax para iniciar sesión
"""
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from django.contrib.auth import authenticate, login


@dajaxice_register()
def iniciar(request, user, passwd):
    dajax = Dajax()
    if request.method == 'POST':
        this_user = authenticate(username=user, password=passwd)
        if this_user and this_user.is_active:
            login(request, this_user)
            try:
                dajax.redirect(request.session['url_next'])
            except Exception as e:
                print e
                dajax.redirect('/')
        else:
            dajax.remove_css_class('#errorlogin', 'novisible')
            dajax.add_css_class('#validalogin', 'novisible')
            dajax.add_css_class('#formulario-login', 'animated shake frmlogin')

    return dajax.json()