# -*- coding: utf-8 -*-

from django.contrib import admin
from apps.personas.models import Alumno, Docente, Curso, Matricula


admin.site.register(Alumno)
admin.site.register(Docente)
admin.site.register(Curso)
admin.site.register(Matricula)
