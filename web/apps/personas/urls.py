# -*- coding: UTF-8 -*-

# 3rd party imports
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(
        r'^$',
        'apps.personas.views.usuarios_home',
        name='usuarios_home'
    ),
    )
