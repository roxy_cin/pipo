# -*- coding: utf-8 -*-
from dajax.core import Dajax

from dajaxice.decorators import dajaxice_register
from apps.personas.models import Alumno, Docente, Curso


@dajaxice_register()
def existe_alumno(request, codigo):
    dajax = Dajax()

    alumno = Alumno.objects.filter(codigo__icontains=codigo)
    if alumno:
        alumno = alumno[0]
        dajax.assign('#id_nombres', 'value', alumno.nombres)
        dajax.assign('#id_apellido_paterno', 'value', alumno.apellido_paterno)
        dajax.assign('#id_apellido_materno', 'value', alumno.apellido_materno)
        dajax.assign('#id_sexo', 'value', alumno.sexo)
        dajax.assign('#id_correo', 'value', alumno.correo)
        dajax.remove_css_class('#alumnoexiste', 'novisible')
        dajax.add_css_class('#alumnoexiste', 'animated fadeInUp')
    else:
        dajax.assign('#id_nombres', 'value', '')
        dajax.assign('#id_apellido_paterno', 'value', '')
        dajax.assign('#id_apellido_materno', 'value', '')
        dajax.assign('#id_sexo', 'value', '')
        dajax.assign('#id_correo', 'value', '')
        dajax.remove_css_class('#alumnoexiste', 'animated flipInX')
        dajax.add_css_class('#alumnoexiste', 'novisible')

    return dajax.json()


@dajaxice_register()
def existe_docente(request, codigo):
    dajax = Dajax()

    docente = Docente.objects.filter(codigo__icontains=codigo)
    if docente:
        docente = docente[0]
        dajax.assign('#id_nombres', 'value', docente.nombres)
        dajax.assign('#id_apellido_paterno', 'value', docente.apellido_paterno)
        dajax.assign('#id_apellido_materno', 'value', docente.apellido_materno)
        dajax.assign('#id_correo', 'value', docente.correo)
        dajax.remove_css_class('#docenteexiste', 'novisible')
        dajax.add_css_class('#docenteexiste', 'animated fadeInUp')
    else:
        dajax.assign('#id_nombres', 'value', '')
        dajax.assign('#id_apellido_paterno', 'value', '')
        dajax.assign('#id_apellido_materno', 'value', '')
        dajax.assign('#id_correo', 'value', '')
        dajax.remove_css_class('#docenteexiste', 'animated flipInX')
        dajax.add_css_class('#docenteexiste', 'novisible')

    return dajax.json()


@dajaxice_register()
def existe_curso(request, codigo):
    dajax = Dajax()

    curso = Curso.objects.filter(codigo__icontains=codigo)
    if curso:
        curso = curso[0]
        dajax.assign('#id_nombre', 'value', curso.nombre)
        dajax.remove_css_class('#cursoexiste', 'novisible')
        dajax.add_css_class('#cursoexiste', 'animated fadeInUp')
    else:
        dajax.assign('#id_nombre', 'value', '')
        dajax.remove_css_class('#cursoexiste', 'animated flipInX')
        dajax.add_css_class('#cursoexiste', 'novisible')

    return dajax.json()