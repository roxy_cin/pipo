# -*- coding: UTF-8 -*-

# 3rd party imports
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^$', 'apps.pipoadmin.views.admin_home'),
    url(r'^perfil/passwd/$', 'apps.pipoadmin.views.admin_editar_passwd'),
    # -- alumnos
    url(r'^alumno/agregar/$', 'apps.pipoadmin.views.alumnos_agregar'),
    url(r'^alumnos/importar/$', 'apps.pipoadmin.views.alumnos_importar'),
    url(r'^alumnos/listar/$', 'apps.pipoadmin.views.alumnos_listar'),
    url(r'^alumnos/editar/(?P<codigo_alumno>\w+)/$',
        'apps.pipoadmin.views.alumno_editar'),
    url(r'^alumnos/eliminar/$', 'apps.pipoadmin.views.alumno_eliminar'),

    # -- docentes
    url(r'^docente/agregar/$', 'apps.pipoadmin.views.docente_agregar'),
    url(r'^docentes/importar/$', 'apps.pipoadmin.views.docentes_importar'),
    url(r'^docentes/listar/$', 'apps.pipoadmin.views.docentes_listar'),
    url(r'^docentes/editar/(?P<codigo_docente>\w+)/$',
        'apps.pipoadmin.views.docente_editar'),
    url(r'^docentes/eliminar/$', 'apps.pipoadmin.views.docente_eliminar'),

    # -- cursos
    url(r'^curso/agregar/$', 'apps.pipoadmin.views.curso_agregar'),
    url(r'^curso/eliminar/$', 'apps.pipoadmin.views.curso_eliminar'),
    url(r'^cursos/importar/$', 'apps.pipoadmin.views.cursos_importar'),
    url(r'^cursos/listar/$', 'apps.pipoadmin.views.cursos_listar'),
    url(r'^cursos/editar/(?P<codigo_curso>\w+)/$',
        'apps.pipoadmin.views.cursos_editar'),

    # -- matrículas
    url(r'^matricula/importar/$', 'apps.pipoadmin.views.matricula_importar'),
    url(r'^matricula/agregar/$', 'apps.pipoadmin.views.matricula_agregar'),
    url(r'^matricula/eliminar/$', 'apps.pipoadmin.views.matricula_eliminar'),
    url(r'^matricula/alumno/eliminar/$',
        'apps.pipoadmin.views.matricula_eliminar_alumno'),
    url(r'^matricula/listar/$', 'apps.pipoadmin.views.matricula_listar'),
    url(r'^matricula/detalle/(?P<semestre>\d+\-\w+)/(?P<codigo_curso>\w+)/$',
        'apps.pipoadmin.views.matricula_detalle'),

    # -- cuestionario
    url(r'^cuestionario/crear/$', 'apps.pipoadmin.views.cuestionario_crear'),
    url(r'^cuestionario/ver/$', 'apps.pipoadmin.views.cuestionario_ver'),
    url(r'^cuestionario/crear/(?P<id_cuestionario>\d*)/$',
        'apps.pipoadmin.views.cuestionario_crear'),
    url(r'^cuestionario/crear/(?P<id_cuestionario>\d*)/instrucciones/$',
        'apps.pipoadmin.views.cuestionario_instrucciones'),
    url(r'^cuestionario/eliminar/(?P<id_cuestionario>\d+)/$',
        'apps.pipoadmin.views.cuestionario_eliminar'),

    # -- categorias
    url(r'^cuestionario/categoria/agregar/$',
        'apps.pipoadmin.views.categoria_agregar'),
    url(
        r'^cuestionario/categoria/editar/(?P<id_cuestionario>\d+)/(?P<id_categoria>\d+)/$',
        'apps.pipoadmin.views.categoria_editar'),
    url(r'^cuestionario/categoria/eliminar/$',
        'apps.pipoadmin.views.categoria_eliminar'),
    url(r'^cuestionario/categoria/(?P<id_categoria>\d+)/diapos/$',
        'apps.pipoadmin.views.categoria_diapos'),
    url(
        r'^cuestionario/categoria/(?P<id_categoria>\d+)/diapos/(?P<id_diapo>\d+)/$',
        'apps.pipoadmin.views.categoria_diapos'),
    url(r'^cuestionario/categoria/(?P<id_categoria>\d+)/diapo/eliminar/$',
        'apps.pipoadmin.views.categoria_diapo_eliminar'),
    url(r'^cuestionario/categoria/(?P<id_categoria>\d+)/consejos/$',
        'apps.pipoadmin.views.categoria_consejos'),

    # -- preguntas
    url(r'^cuestionario/pregunta/agregar/$',
        'apps.pipoadmin.views.pregunta_agregar'),
    url(r'^cuestionario/pregunta/eliminar/$',
        'apps.pipoadmin.views.pregunta_eliminar'),
    url(
        r'^cuestionario/(?P<id_cuestionario>\d+)/pregunta/editar/(?P<id_pregunta>\d+)/$',
        'apps.pipoadmin.views.pregunta_editar'),

    # -- alternativas
    url(r'^cuestionario/alternativa/agregar/$',
        "apps.pipoadmin.views.alternativa_agregar"),
    url(r'^cuestionario/alternativa/eliminar/$',
        'apps.pipoadmin.views.alternativa_eliminar'),
    url(
        r'^cuestionario/(?P<id_cuestionario>\d+)/alternativa/editar/(?P<id_alternativa>\d+)/$',
        'apps.pipoadmin.views.alternativa_editar'),

    # -- reportes
    url(r'^reporte/curso/(?P<codigo_curso>\w+)/$',
        'apps.docente.views.reporte_curso_cuestionario'),
)