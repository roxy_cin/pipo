# -*-  coding: utf-8 -*-
"""
Formulario para alumnos
"""
from django.forms.models import ModelForm
from django.forms.widgets import TextInput, HiddenInput
from apps.personas.models import Alumno


class AlumnoFormAdmin(ModelForm):
    class Meta:
        model = Alumno
        widgets = {
            'codigo': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'nombres': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;'
                }
            ),
            'apellido_paterno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;'
                }
            ),
            'apellido_materno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;'
                }
            ),
            'carrera': HiddenInput(),
            'usuario': HiddenInput(),
        }


class AlumnoFormAgregar(ModelForm):
    class Meta:
        model = Alumno
        widgets = {
            'codigo': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True,
                    'onKeyUp': "buscarAlumno();"
                }
            ),
            'nombres': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True
                }
            ),
            'apellido_paterno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True
                }
            ),
            'apellido_materno': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True
                }
            ),
            'carrera': TextInput(
                attrs={
                    'style': 'text-transform:uppercase;',
                    'required': True
                }
            ),
            'correo': TextInput(
                attrs={
                    'required': True
                }
            ),
        }
        exclude = 'usuario'


class AlumnoForm(ModelForm):
    class Meta:
        model = Alumno
        widgets = {
            'codigo': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'nombres': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'apellido_paterno': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'apellido_materno': TextInput(
                attrs={
                    'readonly': ''
                }
            ),
            'carrera': HiddenInput(),
            'usuario': HiddenInput(),
        }