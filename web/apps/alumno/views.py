# -*- coding: utf-8 -*-
"""
Vistas de la aplicación docente
"""
# 3rd
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render_to_response
from django.template import RequestContext
# local
from apps.alumno.forms import AlumnoForm
from apps.personas.models import Alumno


@user_passes_test(lambda u: u.groups.filter(name='alumno').count() == 1,
                  login_url='/')
def alumno_editar_perfil(request):
    """
    Editar datos del alumno
    """
    mensaje = ''
    # Recuperar el alumno que inició sesión
    usuario = request.user
    alumno = Alumno.objects.get(codigo=usuario)
    # Mostrar formulario de edición de datos
    alumno_form = AlumnoForm(instance=alumno)

    if request.method == 'POST':
        alumno_form = AlumnoForm(request.POST, instance=alumno)
        if alumno_form.is_valid():
            alumno_form.save()
            mensaje = 'Datos actualizados correctamente'

    data = {
        'alumno_form': alumno_form,
        'mensaje': mensaje
    }
    return render_to_response('alumno_editar_perfil.html', data,
                              context_instance=RequestContext(request))


@user_passes_test(lambda u: u.groups.filter(name='alumno').count() == 1,
                  login_url='/')
def alumno_editar_passwd(request):
    if request.method == 'POST':
        mensaje_passwd_er = mensaje_passwd_ok = None
        # Recuperar el alumno que inició sesión
        alumno = Alumno.objects.get(codigo=request.user)

        # Recuperar contraseña anterior
        password = request.POST.get('passwd_ant')
        # Verificar si es la contraseña correcta
        if authenticate(username=request.user.username, password=password):
            # recuperar la nueva contraseña ingresada
            nuevo_passwd1 = request.POST.get('passwd_nueva1')
            nuevo_passwd2 = request.POST.get('passwd_nueva2')
            if nuevo_passwd1 == nuevo_passwd2:
                user = request.user
                user.set_password(nuevo_passwd1)
                user.save()
                mensaje_passwd_ok = u'Contraseña actualizada correctamente'
            else:
                mensaje_passwd_er = u'Las contraseñas ingresadas no coinciden'
        else:
            mensaje_passwd_er = u'La contraseña actual no es válida.'

        data = {
            'mensaje_passwd_er': mensaje_passwd_er,
            'mensaje_passwd_ok': mensaje_passwd_ok,
            'username': request.user.username,
            'plantilla': 'alumno_base.html',
        }
        return render_to_response('cambia_password.html', data,
                                  context_instance=RequestContext(request))
    else:
        data = {
            'plantilla': 'alumno_base.html'
        }
        return render_to_response('cambia_password.html', data,
                                  context_instance=RequestContext(request))