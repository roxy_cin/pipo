# -*- coding: utf-8 -*-
"""
Funciones útiles para el sistema
"""

# 3rd part imports
from chartit import PivotDataPool, PivotChart
from django.db.models import Avg
from openpyxl import load_workbook
# Local imports
from apps.personas.models import Alumno, Docente, Curso, CarreraProfesional
from apps.personas.models import Matricula


def importar_alumno_archivo(archivo):
    """
    Cargar el archivo e importar su contenido a la BD

    :param archivo: Archivo .xlsx con los datos a cargar
    """
    # Subir el archivo a la carpeta temporal para importar
    destination = open('/tmp/%s' % archivo, 'wb+')
    for chunk in archivo.chunks():
        destination.write(chunk)
    destination.close()
    # Abrir el archivo
    try:
        wb = load_workbook(filename='/tmp/%s' % archivo, use_iterators=True)
        # Abrir la hoja
        ws = wb.get_sheet_by_name(name='Hoja1')

        # Lista donde almacenar alumnos importados
        alumnos_importados = []
        # Leer las filas del archivo, las celdas que nos
        # interesan recuperar son, CODIGO [0], NOMBRE[1]
        # CP[2], CORREO[3], SEXO[4]
        for row in ws.iter_rows():
            if row[0].internal_value is None:
                break

            codigo = "%06d" % int(row[0].internal_value)
            # Ver si ya existe alumno en la base de datos
            try:
                if not Alumno.objects.filter(codigo=codigo).count():
                    apellido_paterno = row[1].internal_value.split("-")[0]
                    apellido_materno = row[1].internal_value.split("-")[1]
                    nombres = row[1].internal_value.split("-")[2]
                    carrera = row[2].internal_value
                    carrera, new = CarreraProfesional.objects.get_or_create(
                        codigo=carrera, nombre=carrera)
                    correo = row[3].internal_value
                    if correo == 'NULL':
                        correo = None

                    sexo = str(int(row[4].internal_value))
                    # Guardar el alumno
                    alumno = Alumno(
                        codigo=codigo,
                        apellido_paterno=apellido_paterno,
                        apellido_materno=apellido_materno,
                        nombres=nombres,
                        sexo=sexo,
                        correo=correo,
                        carrera=carrera,
                        usuario=None,
                    )
                    alumno.save()
                    alumnos_importados.append(alumno)

            except Exception as e:
                print e

        return alumnos_importados

    except Exception as e:
        print 'error', e.message
        return None


def importar_docente_archivo(archivo):
    """
    Cargar el archivo e importar su contenido a la BD
    :param archivo: Archivo desde el que se cargarán los datos
    """
    # Cargar el archivo a la carpeta temporal, para importarlo
    destination = open('/tmp/%s' % archivo, 'wb+')
    for chunk in archivo.chunks():
        destination.write(chunk)
    destination.close()
    try:
        # Abrir el archivo
        wb = load_workbook(filename='/tmp/%s' % archivo, use_iterators=True)
        # Abrir la hoja
        ws = wb.get_sheet_by_name(name='Hoja1')

        # Lista donde almacenar docentes importados
        docentes_importados = []

        # Leer las filas del archivo, las celdas que nos
        # interesan recuperar son, CODIGO [0], NOMBRE[1]
        # CORREO[2]
        for row in ws.iter_rows():
            if row[0].internal_value is None:
                break

            try:
                codigo = "%05d" % int(row[0].internal_value)
                # Ver si ya existe docente en la base de datos
                if not Docente.objects.filter(codigo=codigo).count():
                    apellido_paterno = row[1].internal_value.split("-")[0]
                    apellido_materno = row[1].internal_value.split("-")[1]
                    nombres = row[1].internal_value.split("-")[2]
                    correo = row[2].internal_value
                    # crear usuario
                    docente = Docente(
                        codigo=codigo,
                        apellido_paterno=apellido_paterno,
                        apellido_materno=apellido_materno,
                        nombres=nombres,
                        correo=correo,
                        usuario=None
                    )
                    # guardar el docente
                    docente.save()
                    docentes_importados.append(docente)

            except Exception as e:
                print e

        return docentes_importados

    except Exception as e:
        print 'error', e
        return None


def importar_cursos_archivo(archivo):
    """
    Cargar el archivo e importar su contenido a la BD
    :param archivo: Archivo desde el que se cargarán los datos
    """
    # Cargar el archivo a la carpeta temporal, para importarlo
    destination = open('/tmp/%s' % archivo, 'wb+')
    for chunk in archivo.chunks():
        destination.write(chunk)
    destination.close()

    try:
        # Abrir el archivo
        wb = load_workbook(filename='/tmp/%s' % archivo, use_iterators=True)
        # Abrir la hoja
        ws = wb.get_sheet_by_name(name='Hoja1')

        # Lista donde almacenar cursos importados
        cursos_importados = []
        # Leer las filas del archivo, las celdas que nos
        # interesan recuperar son, CODIGO[0], NOMBRE[1]
        for row in ws.iter_rows():
            if row[0].internal_value is None:
                break

            codigo = row[0].internal_value
            # Ver si ya existe curso en la base de datos
            if not Curso.objects.filter(codigo=codigo).count():
                nombre = row[1].internal_value
                curso = Curso(
                    codigo=codigo,
                    nombre=nombre,
                )
                # guardar el curso
                curso.save()
                cursos_importados.append(curso)

        return cursos_importados

    except Exception as e:
        print 'error', e.message
        return False


def importar_matricula_archivo(archivo):
    """
    Cargar el archivo e importar su contenido a la BD
    :param archivo: Archivo desde el que se cargarán los datos
    """
    # Cargar el archivo a la carpeta temporal, para importarlo
    destination = open('/tmp/%s' % archivo, 'wb+')
    for chunk in archivo.chunks():
        destination.write(chunk)
    destination.close()

    try:
        # Abrir el archivo
        wb = load_workbook(filename='/tmp/%s' % archivo, use_iterators=True)
        # Abrir la hoja
        ws = wb.get_sheet_by_name(name='Hoja1')

        # Lista donde almacenar matriculas importadas
        matriculas_importadas = []
        # Leer las filas del archivo, las celdas que nos
        # interesan recuperar son, ALUMNO[0], CURSO[1], DOCENTE[2], SEMESTRE[3]
        for row in ws.iter_rows():
            try:
                if row[0].internal_value is None:
                    break

                alumno = "%06d" % int(row[0].internal_value)
                curso = str(row[1].internal_value)
                docente = "%05d" % int(row[2].internal_value)
                semestre = str(row[3].internal_value)

                # Ver si ya existe matricula en la base de datos
                if not Matricula.objects.filter(
                        curso__codigo=curso, alumno__codigo=alumno,
                        docente__codigo=docente, semestre=semestre).count():
                    matricula = Matricula(
                        curso=Curso.objects.get(codigo=curso),
                        alumno=Alumno.objects.get(codigo=alumno),
                        docente=Docente.objects.get(codigo=docente),
                        semestre=semestre
                    )
                    # guardar la matricula
                    matricula.save()
                    matriculas_importadas.append(matricula)

            except Exception as e:
                print e

        return matriculas_importadas

    except Exception as e:
        print 'error', e.message
        return False


def grafico_reporte(datos, curso, tipo_grafico):
    """
    Generar gráfico

    :param datos:  Datos con los cuales formar el gráfico
    :param tipo_grafico:  Tipo de gráfico, puede ser
    """
    ds = PivotDataPool(
        series=[
            {
                'options': {
                    'source': datos,
                    'categories': [u'categoria__nombre']
                },
                'terms': {
                    'Cantidad': Avg('cantidad'),
                }
            }
        ]
    )
    nombre_curso = curso.nombre if curso else ''

    cht = PivotChart(
        datasource=ds,
        series_options=[
            {
                'options': {
                    'type': tipo_grafico,
                    'stacking': True
                },
                'terms': ['Cantidad'],
            }
        ],
        chart_options={
            'title': {
                'text': '<b>RESULTADOS %s</b>' % nombre_curso,
            },
            'xAxis': {
                'title': {
                    'text': u'Categoría'
                }
            },
        },
    )

    return cht